import expect from 'expect';
import * as types from '../../src/constants/gistsConstants';
import reducer from '../../src/reducers/gists';

describe('gist reducer', () => {
  it('should return an object with an empty array under the key boxes (initial state)', () => {
    expect(
      reducer(undefined, {})
    ).toEqual({
      isFetching: false,
      ownGists: [],
      starredGists: [],
      currentGist: null,
      labels: {},
      gist: null,
      searchGist: ""
    });
  });

  it('should add gists and attach to them empty labels', () => {
    expect(
      reducer(
        {
          ownGists: [],
          starredGists: [],
          labels: {}
        },
        {
          type: types.LIST_FULFILLED,
          payload: [[{
            'id': '12345678'
          }], [{
            'id': '87654321'
          }]]
        }
      )
    ).toEqual(
      {
        isFetching: false,
        labels: {},
        ownGists: [{
          'id': '12345678',
          'label': ""
        }],
        starredGists: [{
          'id': '87654321',
          'label': ""
        }]
      }
    );
  });
});
