import expect from 'expect';
import * as types from '../../src/constants/userConstants';
import reducer from '../../src/reducers/user';

describe('user reducer', () => {
  it('should return an object with an empty array under the key boxes (initial state)', () => {
    expect(
      reducer(undefined, {})
    ).toEqual({
      isAuthenticated: false,
      token: null
    });
  });

  it('should add token to store', () => {
    expect(
      reducer(
        {
          isAuthenticated: false,
          token: null
        },
        {
          type: types.USER_LOGIN,
          payload: {
            'token': '12345678'
          }
        }
      )
    ).toEqual({
          isAuthenticated: true,
          token: '12345678'
        }
    );
  });
});
