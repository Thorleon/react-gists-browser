import expect from 'expect';
import * as actions from '../../src/actions/gistsActions';
import * as types from '../../src/constants/gistsConstants';

describe('gist actions', () => {
  it('should create an action to change the search', () => {
    const searchGist = 'Amazing content!!';
    const expectedAction = {
      type: types.CHANGE_SEARCH,
      payload: {
        searchGist
      }
    };
    expect(
      actions.changeSearch(searchGist)
    ).toEqual(expectedAction);
  });
});
