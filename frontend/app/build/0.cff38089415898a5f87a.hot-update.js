webpackHotUpdate(0,{

/***/ 565:
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(module) {/* REACT HOT LOADER */ if (true) { (function () { var ReactHotAPI = __webpack_require__(3), RootInstanceProvider = __webpack_require__(11), ReactMount = __webpack_require__(13), React = __webpack_require__(82); module.makeHot = module.hot.data ? module.hot.data.makeHot : ReactHotAPI(function () { return RootInstanceProvider.getRootInstances(ReactMount); }, React); })(); } try { (function () {
	
	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.logoutUser = exports.loginUser = undefined;
	
	var _userConstants = __webpack_require__(351);
	
	var types = _interopRequireWildcard(_userConstants);
	
	function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }
	
	var loginUser = exports.loginUser = function loginUser(token) {
	  localStorage.setItem('token', token);
	  return {
	    type: types.USER_LOGIN,
	    payload: {
	      'token': token
	    }
	  };
	  // return fetch(`https://github.com/login/oauth/access_token?client_id=576899d00454f9480f56&client_secret=79bbb9a4a88669229e4efbe49a42132d6b493ede&code=${code}`, {
	  //  method: 'POST',
	  //  mode: 'no-cors',
	  //  headers: headers
	  //}).then(response => response.json())
	  //  .then(json => dispatch(receiveLogin(json)))
	};
	
	var logoutUser = exports.logoutUser = function logoutUser() {
	  localStorage.setItem('token', null);
	  return types.USER_LOGOUT;
	};
	
	/* REACT HOT LOADER */ }).call(this); } finally { if (true) { (function () { var foundReactClasses = module.hot.data && module.hot.data.foundReactClasses || false; if (module.exports && module.makeHot) { var makeExportsHot = __webpack_require__(347); if (makeExportsHot(module, __webpack_require__(82))) { foundReactClasses = true; } var shouldAcceptModule = true && foundReactClasses; if (shouldAcceptModule) { module.hot.accept(function (err) { if (err) { console.error("Cannot not apply hot update to " + "userActions.js" + ": " + err.message); } }); } } module.hot.dispose(function (data) { data.makeHot = module.makeHot; data.foundReactClasses = foundReactClasses; }); })(); } }
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(2)(module)))

/***/ }

})
//# sourceMappingURL=0.cff38089415898a5f87a.hot-update.js.map