import React from 'react';
import { Router, Route, IndexRoute } from 'react-router';
import { Provider } from 'react-redux';
import store, { history } from '../store/store';
import App from '../components/App';
import * as Pages from '../pages/';
import ReduxToastr from 'react-redux-toastr';
import * as Containers from '../containers/';

const router = (
  <Provider store={store}>
    <div>
      <ReduxToastr
        timeOut={4000}
        newestOnTop
        position="bottom-right"
      />
      <Router history={history}>
        <Route path="/" component={App}>
          <IndexRoute component={Containers.determineAuth(Pages.LandingPage)} />
          <Route path="/login" component={Containers.noReqAuth(Pages.LoginPage)} />
          <Route path="/list" component={Containers.reqAuth(Pages.ListPage)} />
          <Route path="/new" component={Containers.reqAuth(Pages.NewPage)} />
          <Route path="/edit/:gistId" component={Containers.reqAuth(Pages.EditPage)} />
          <Route path="/redirect" component={Pages.RedirectPage} />
        </Route>
      </Router>
    </div>
  </Provider>
);

export default router;
