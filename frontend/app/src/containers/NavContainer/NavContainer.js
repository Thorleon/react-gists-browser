import React, { PropTypes, Component } from 'react';
import styles from './NavContainer.module.scss';
import cssModules from 'react-css-modules';
import { Link } from 'react-router';
import * as userActions from '../../actions/userActions';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';

import { bindActionCreators } from 'redux';

class NavContainer extends Component {

  constructor(props) {
    super(props);
    this.logoutHandler = this.logoutHandler.bind(this);
  }

  logoutHandler() {
    const { actions } = this.props;
    actions.logoutUser();
  }

  render() {
    return (
      <nav className={styles.nav}>
        <Link to="/">Home</Link>
          {!this.props.isAuthenticated ?
              <Link to="/login">Login</Link>
              :
              <div>
                <Link to="/new">New</Link>
                <Link to="/list">List</Link>
                <Link onClick={this.logoutHandler}>Logout</Link>
              </div>
          }
      </nav>
    );
  }
}

NavContainer.propTypes = {
  isAuthenticated: PropTypes.bool.isRequired
};

const mapStateToProps = (state) => ({
  isAuthenticated: state.user.isAuthenticated
});

const mapDispatchToProps = (dispatch) => ({
  actions: bindActionCreators(userActions, dispatch)
});

const StyledComponent = cssModules(NavContainer, styles);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(StyledComponent);
