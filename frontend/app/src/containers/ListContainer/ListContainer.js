import React, { PropTypes, Component } from 'react';
import styles from './ListContainer.module.scss';
import cssModules from 'react-css-modules';
import { GistsListComponent, ViewComponent, SearchInput } from 'components';
import * as gistsActions from '../../actions/gistsActions';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';


class ListContainer extends Component {

  constructor(props) {
    super(props);
    this.changeCurrentGist = this.changeCurrentGist.bind(this);
    this.searchType = this.searchType.bind(this);
    this.removeGist = this.removeGist.bind(this);
  }

  componentDidMount() {
    const { actions } = this.props;
    actions.fetchList();
  }

  changeCurrentGist(gistId) {
    console.log(gistId);
    const { actions } = this.props;
    actions.changeCurrentGist(gistId);
    actions.fetchGist(gistId);
  }

  removeGist(gistId) {
    const { actions } = this.props;
    actions.deleteGist(gistId);
  }

  searchType(text) {
    const { actions } = this.props;
    actions.changeSearch(text);
  }

  render() {
    return (
      <div className={styles.sectionCenterRight}>
        <section className={styles.sectionCenter}>
          <SearchInput searchInput={this.searchType}/>
          <GistsListComponent search={this.props.gists.searchGist}
                              gists={this.props.gists}
                              changeCurrentGist={this.changeCurrentGist}
                              removeGist={this.removeGist}/>
        </section>
        {this.props.gists.currentGist && this.props.gists.gist ?
          <ViewComponent {...this.props.gists.gist}/> :
          null
        }
      </div>
    );
  }
}

ListContainer.propTypes = {
  ownGists: PropTypes.array,
  isAuthenticated: PropTypes.bool.isRequired
};


const mapStateToProps = (state) => ({
  gists: state.gists
});


const mapDispatchToProps = (dispatch) => ({
  actions: bindActionCreators(gistsActions, dispatch)
});


const StyledComponent = cssModules(ListContainer, styles);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(StyledComponent);
