import React, { PropTypes, Component } from 'react';
import styles from './RedirectContainer.module.scss';
import cssModules from 'react-css-modules';
import * as userActions from '../../actions/userActions';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';
import { bindActionCreators } from 'redux';


class RedirectContainer extends Component {

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    const githubToken = this.props.location.query.token;
    const { actions } = this.props;
    actions.loginUser(githubToken);
    browserHistory.push('/list');
  }

  render() {
    return (
      <div className={styles.nav}>
        <p>You will be redirect soon.</p>
      </div>
    );
  }
}

RedirectContainer.propTypes = {
  isAuthenticated: PropTypes.bool.isRequired
};


const mapStateToProps = (state) => ({
  isAuthenticated: state.user.isAuthenticated
});


const mapDispatchToProps = (dispatch) => ({
  actions: bindActionCreators(userActions, dispatch)
});


const StyledComponent = cssModules(RedirectContainer, styles);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(StyledComponent);
