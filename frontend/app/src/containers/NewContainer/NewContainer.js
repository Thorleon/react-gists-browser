import React, { PropTypes, Component } from 'react';
import styles from './NewContainer.module.scss';
import cssModules from 'react-css-modules';
import { FormComponent } from 'components';
import * as gistsActions from '../../actions/gistsActions';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';


class NewContainer extends Component {

  constructor(props) {
    super(props);
    this.defaultGist = {
      'description': 'Description',
      'public': 'true',
      'label': 'Label',
      'files': {
        'example.txt': {
          'content': 'Lorem ipsum'
        }
      }
    };
  }

  addGist(state) {
    const { actions } = this.props;
    actions.newGist(state);
  }


  render() {
    return (
      <div className={styles.nav}>
        <FormComponent onSubmit={this.addGist.bind(this)} gist={this.defaultGist} description="Description"/>
      </div>
    );
  }
}

NewContainer.propTypes = {
  isAuthenticated: PropTypes.bool.isRequired
};


const mapStateToProps = (state) => ({
  gists: state.gists,
  isAuthenticated: state.user.isAuthenticated
});


const mapDispatchToProps = (dispatch) => ({
  actions: bindActionCreators(gistsActions, dispatch)
});


const StyledComponent = cssModules(NewContainer, styles);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(StyledComponent);
