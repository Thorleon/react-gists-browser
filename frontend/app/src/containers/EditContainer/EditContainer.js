import React, { PropTypes, Component } from 'react';
import styles from './EditContainer.module.scss';
import cssModules from 'react-css-modules';
import { FormComponent } from 'components';
import * as gistsActions from '../../actions/gistsActions';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';


class EditContainer extends Component {

  constructor(props) {
    super(props);
    this.gistId = props.params.gistId;
    const currentGist = props.gists.ownGists.filter(gist => gist.id == this.gistId)[0];
    this.gist = currentGist;
  }



  editGist(state) {
    const { actions } = this.props;
    const editedGist = {
      'description': state.description,
      'label': state.label,
      'public': 'true',
      'files': state.files
    };
    actions.editGist(editedGist, state.id);
  }

  render() {
    return (
      <div className={styles.nav}>
        <FormComponent gist={this.gist} onSubmit={this.editGist.bind(this)} />
      </div>
    );
  }
}

EditContainer.propTypes = {
  isAuthenticated: PropTypes.bool.isRequired
};


const mapStateToProps = (state) => ({
  gists: state.gists,
  isAuthenticated: state.user.isAuthenticated
});


const mapDispatchToProps = (dispatch) => ({
  actions: bindActionCreators(gistsActions, dispatch)
});


const StyledComponent = cssModules(EditContainer, styles);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(StyledComponent);
