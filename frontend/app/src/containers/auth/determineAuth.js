import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as userActions from '../../actions/userActions';

const mapStateToProps = (state) => ({
    token: state.user.token,
    isAuthenticated: state.user.isAuthenticated
});


const mapDispatchToProps = (dispatch) => ({
    actions: bindActionCreators(userActions, dispatch)
});


const determineAuth = (Component) => {

    class DetermineAuthComponent extends React.Component {

        componentWillMount = () => {
            this.checkAuth();
        };

        componentWillReceiveProps = (nextProps) => {
            this.checkAuth(nextProps);
        };

        checkAuth = (props = this.props) => {
            if (!props.isAuthenticated) {
                const token = localStorage.getItem('token');
                if (token) {
                    const { actions } = this.props;
                    actions.loginUser(token);
                }
            }
        };

        render = () => (
            <div>
                <Component {...this.props} />
            </div>
        );
    }

    DetermineAuthComponent.propTypes = {
        isAuthenticated: React.PropTypes.bool
    };

    return connect(mapStateToProps, mapDispatchToProps)(DetermineAuthComponent);
};

export default determineAuth;
