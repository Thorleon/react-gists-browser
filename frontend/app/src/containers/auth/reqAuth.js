import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { browserHistory } from 'react-router';
import * as userActions from '../../actions/userActions';

const mapStateToProps = (state) => ({
    token: state.user.token,
    isAuthenticated: state.user.isAuthenticated
});


const mapDispatchToProps = (dispatch) => ({
    actions: bindActionCreators(userActions, dispatch)
});


const reqAuth = (Component) => {

    class AuthenticatedComponent extends React.Component {

        componentWillMount = () => {
            this.checkAuth();
        };

        componentWillReceiveProps = (nextProps) => {
            this.checkAuth(nextProps);
        };

        checkAuth = (props = this.props) => {
            if (!props.isAuthenticated) {
                const token = localStorage.getItem('token');
                if (!token) {
                    browserHistory.push('/login');
                } else {
                    const { actions } = this.props;
                    actions.loginUser(token);
                }
            }
        };

        render = () => (
            <div>
                {this.props.isAuthenticated
                    ? <Component {...this.props} />
                    : null
                }
            </div>
        );
    }

    AuthenticatedComponent.propTypes = {
        isAuthenticated: React.PropTypes.bool
    };

    return connect(mapStateToProps, mapDispatchToProps)(AuthenticatedComponent);
};

export default reqAuth;
