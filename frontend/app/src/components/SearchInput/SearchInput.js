import React, { PropTypes, Component } from 'react';
import cssModules from 'react-css-modules';
import styles from './SearchInput.module.scss';

class SearchInput extends Component {

  constructor(props) {
    super(props);
    this.handleInput = this.handleInput.bind(this);
  }

  handleInput(e) {
    this.props.searchInput(e.target.value);
  }

  render() {
    return (
      <div className={styles.formGroup}>
        <label>Search input</label>
        <input onChange={this.handleInput}
               type="text"/>
      </div>
    );
  }
}

SearchInput.propTypes = {
  onSubmit: PropTypes.func.isRequired
};

export default cssModules(SearchInput, styles);
