import React, { PropTypes, Component } from 'react';
import cssModules from 'react-css-modules';
import styles from './ViewFileRow.module.scss';

class ViewFileRow extends Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <article className={styles.row}>
        <h5>{this.props.filename}</h5>
        <a href={this.props.raw_url}>Go to raw</a>
      </article>
    );
  }
}

ViewFileRow.propTypes = {

};

export default cssModules(ViewFileRow, styles);
