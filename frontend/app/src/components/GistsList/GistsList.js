import React, { PropTypes, Component } from 'react';
import cssModules from 'react-css-modules';
import styles from './GistsList.module.scss';


import { GistRow } from 'components';

class GistsList extends Component {

  constructor() {
    super();
    this.changeCurrentGist = this.changeCurrentGist.bind(this);
    this.removeGist = this.removeGist.bind(this);
  }

  changeCurrentGist(id) {
    this.props.changeCurrentGist(id);
  }

  removeGist(id) {
    this.props.removeGist(id);
  }

  render() {

    return (
      <section>
        <h2>{this.props.title}</h2>
        {this.props.gists.map((gist, i) =>
          <GistRow allowEdit={this.props.allowEdit} changeCurrentGist={this.changeCurrentGist} removeGist={this.removeGist} {...gist}/>)}
      </section>
    );
  }
}

GistsList.propTypes = {
  title: PropTypes.string
};

export default cssModules(GistsList, styles);
