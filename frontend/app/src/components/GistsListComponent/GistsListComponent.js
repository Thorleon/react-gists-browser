import React, { PropTypes, Component } from 'react';
import cssModules from 'react-css-modules';
import styles from './GistsListComponent.module.scss';

import { GistsList } from 'components';


class GistsListComponent extends Component {

  constructor(props) {
    super(props);
    this.removeGist = this.removeGist.bind(this);
    this.changeCurrentGist = this.changeCurrentGist.bind(this);
  }

  changeCurrentGist(id) {
    this.props.changeCurrentGist(id);
  }

  removeGist(id) {
    this.props.removeGist(id);
  }

  filterGists(gists) {
    return this.props.search !== "" ? gists.filter(gist => gist.label === this.props.search) : gists;
  }

  render() {
    const { gists } = this.props;
    return (
      <section className={styles.fullScreen}>
        <GistsList allowEdit={true}
                   title="Own Gists"
                   gists={this.filterGists(gists.ownGists)}
                   removeGist={this.removeGist}
                   changeCurrentGist={this.changeCurrentGist} />
        <GistsList
                   title="Starred Gists"
                   gists={gists.starredGists}
                   changeCurrentGist={this.changeCurrentGist} />
      </section>
    );
  }
}

GistsListComponent.propTypes = {
  gists: PropTypes.object
};

export default cssModules(GistsListComponent, styles);
