import React, { PropTypes, Component } from 'react';
import cssModules from 'react-css-modules';
import styles from './ViewComponent.module.scss';

import { ViewFileRow } from 'components';

class ViewComponent extends Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <section className={styles.formGroup}>
        <h3>Description: {this.props.description}</h3>
        {this.props.label ?
          <p>Label: {this.props.label}</p> :
          null
        }
        <a href={this.props.url}>Go to GitHub</a>
        {Object.keys(this.props.files).map((key, i) =>
          <ViewFileRow {...this.props.files[key]}/>
        )}
      </section>
    );
  }
}

ViewComponent.propTypes = {
  description: PropTypes.string,
  url: PropTypes.string,
  files: PropTypes.object
};

export default cssModules(ViewComponent, styles);
