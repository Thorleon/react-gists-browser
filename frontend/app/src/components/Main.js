import React from 'react';
import cssModules from 'react-css-modules';
import styles from './Main.module.scss';
import { NavContainer } from '../containers';

const Main = (props) => (
  <div className={styles.mainContainer}>
    <NavContainer/>
    {React.cloneElement(props.children, props)}
  </div>
);

export default cssModules(Main, styles);
