import React, { PropTypes, Component } from 'react';
import cssModules from 'react-css-modules';
import styles from './InputRow.module.scss';

class InputRow extends Component {

  constructor(props) {
    super(props);
    this.handleInput = this.handleInput.bind(this);
    this.state = {
      value: this.props.value
    };
  }

  handleInput(e) {
    this.setState({
      value: e.target.value
    });
    this.props.handleInput(this.props.name, e.target.value);
  }

  render() {
    const {
      value
    } = this.state;
    return (
      <div className={styles.formGroup}>
        <label>{this.props.label}</label>
        <input value={value}
               onChange={this.handleInput}
               type={this.props.type}/>
      </div>
    );
  }
}

InputRow.propTypes = {
  onSubmit: PropTypes.func.isRequired
};

export default cssModules(InputRow, styles);
