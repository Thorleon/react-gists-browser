import React, { PropTypes, Component } from 'react';
import cssModules from 'react-css-modules';
import styles from './FormComponent.module.scss';
import update from 'react-addons-update';
import { InputRow, GistFileFormRow } from 'components';

class FormComponent extends Component {

  constructor(props) {
    super(props);
    this.state = props.gist;
    this.handleInput = this.handleInput.bind(this);
    this.handleFileInput = this.handleFileInput.bind(this);
    this.submitHandler = this.submitHandler.bind(this);
    this.addFile = this.addFile.bind(this);
  }

  addFile() {
    const files = this.state.files;
    const fileTitile = `example_${Object.keys(files).length + 1}.txt`;
    const newFile = {};
    newFile[fileTitile] = {'content': 'lorem iposo'};
    const newFiles = update(files, {$merge: newFile});
    const newState = update(this.state, {'files': {$set: newFiles}});
    this.setState(newState);
  }

  submitHandler() {
    this.props.onSubmit(this.state);
  }

  handleInput(name, value) {
    const newState = this.state;
    newState[name] = value;
    this.setState(newState);
  }

  handleFileInput(oldFile, newFile) {
    const files = this.state.files;
    delete files[Object.keys(oldFile)[0]];
    files[Object.keys(newFile)[0]] = newFile[Object.keys(newFile)[0]];
    const newState = this.state;
    newState.files = files;
    this.setState(newState);
  }

  render() {
    return (
      <form>
        <InputRow handleInput={this.handleInput} type="text" name="description" label="Description" value={this.props.gist.description}/>
        <InputRow handleInput={this.handleInput} type="text" name="label" label="Label" value={this.props.gist.label}/>
        {Object.keys(this.state.files).map((key, i) =>
          <GistFileFormRow onChange={this.handleFileInput} fileName={key} content={this.state.files[key].content}/>
        )}
        <a onClick={this.submitHandler}>Save</a>
        <a onClick={this.addFile}>Add file</a>
      </form>
    );
  }
}

FormComponent.propTypes = {
  title: PropTypes.string
};

export default cssModules(FormComponent, styles);
