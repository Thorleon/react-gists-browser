import React, { PropTypes, Component } from 'react';
import cssModules from 'react-css-modules';
import styles from './GistFileFormRow.module.scss';
import { InputRow } from 'components';


class GistFileFormRow extends Component {

  constructor(props) {
    super(props);
    const fileName = props.fileName;
    this.state = {};
    this.state[fileName] = {'content': props.content};
    this.handleFileNameInput = this.handleFileNameInput.bind(this);
    this.handleFileContentInput = this.handleFileContentInput.bind(this);
  }

  handleFileNameInput(name, value) {
    const oldState = this.state;
    this.state = {};
    let newState = {};
    newState[value] = {'content': this.refs.fileContent.state.value};
    this.state = newState;
    this.props.onChange(oldState, newState);
  }

  handleFileContentInput(name, value) {
    const oldState = this.state;
    let newState = {};
    newState[this.refs.fileName.state.value] = {'content': value};
    this.state = newState;
    this.props.onChange(oldState, newState);
  }

  render() {
    return (
      <div>
        <InputRow handleInput={this.handleFileNameInput}
                  ref="fileName"
                  label="File name"
                  name="fileName"
                  type="text"
                  value={this.props.fileName}/>
        <InputRow handleInput={this.handleFileContentInput}
                  ref="fileContent"
                  label="File content"
                  name="fileContent"
                  type="text"
                  value={this.props.content}/>
      </div>
    );
  }
}

GistFileFormRow.propTypes = {

};

export default cssModules(GistFileFormRow, styles);
