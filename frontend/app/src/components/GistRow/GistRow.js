import React, { PropTypes, Component } from 'react';
import cssModules from 'react-css-modules';
import styles from './GistRow.module.scss';
import { Link } from 'react-router';


class GistRow extends Component {

  constructor(props) {
    super(props);
    this.changeCurrentGist = this.changeCurrentGist.bind(this);
    this.removeGist = this.removeGist.bind(this);
  }

  changeCurrentGist() {
    this.props.changeCurrentGist(this.props.id);
  }

  removeGist() {
    this.props.removeGist(this.props.id);
  }

  render() {

    return (
      <article className={styles.gistRow}>
        {this.props.description}
        <div className={styles.buttonWrapper}>
          <Link onClick={this.changeCurrentGist}>View</Link>

          {this.props.allowEdit ? <div>
            <Link to={`/edit/${this.props.id}`}>Edit</Link>
            <Link onClick={this.removeGist}>Delete</Link>
          </div> : null}
        </div>
      </article>
    );
  }
}

GistRow.propTypes = {
  id: PropTypes.number
};

export default cssModules(GistRow, styles);
