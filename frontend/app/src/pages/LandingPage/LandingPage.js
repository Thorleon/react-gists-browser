import React from 'react';
import cssModules from 'react-css-modules';
import styles from './LandingPage.module.scss';



const LandingPage = (props) => (
  <div className={styles.container}>
    <p>React app created only to test purpose.</p>
    <p>Login to see React magic.</p>
    <p>Enjoy</p>
  </div>
);

export default cssModules(LandingPage, styles);
