import React from 'react';
import cssModules from 'react-css-modules';
import styles from './EditPage.module.scss';
import { EditContainer } from '../../containers';

const EditPage = (props) => (
  <div className={styles.container}>
    <EditContainer
      {...props}
    />
  </div>
);

export default cssModules(EditPage, styles);
