import React from 'react';
import cssModules from 'react-css-modules';
import styles from './RedirectPage.module.scss';
import { RedirectContainer } from '../../containers';

// Pages map directly to Routes, i.e. one page equals on Route
// Handler that maps to a route in /utils/routes
const RedirectPage = (props) => (
  <div className={styles.container}>
    <RedirectContainer
      {...props}
    />
  </div>
);

export default cssModules(RedirectPage, styles);
