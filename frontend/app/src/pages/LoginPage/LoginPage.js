import React from 'react';
import cssModules from 'react-css-modules';
import styles from './LoginPage.module.scss';


const LoginPage = () => (
  <div className={styles.container}>
    <a href="https://github.com/login/oauth/authorize?scope=repo%20gist&client_id=576899d00454f9480f56">
      To login click here
    </a>
  </div>
);

export default cssModules(LoginPage, styles);
