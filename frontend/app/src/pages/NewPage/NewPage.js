import React from 'react';
import cssModules from 'react-css-modules';
import styles from './NewPage.module.scss';
import { NewContainer } from '../../containers';

const NewPage = (props) => (
  <div className={styles.container}>
    <NewContainer
      {...props}
    />
  </div>
);

export default cssModules(NewPage, styles);
