import NotFoundPage from './NotFoundPage/NotFoundPage';
import LandingPage from './LandingPage/LandingPage';
import LoginPage from './LoginPage/LoginPage';
import RedirectPage from './RedirectPage/RedirectPage';
import ListPage from './ListPage/ListPage';
import NewPage from './NewPage/NewPage';
import EditPage from './EditPage/EditPage';

export {
  NotFoundPage,
  LandingPage,
  LoginPage,
  RedirectPage,
  ListPage,
  NewPage,
  EditPage
};
