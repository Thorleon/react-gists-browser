import React from 'react';
import cssModules from 'react-css-modules';
import styles from './ListPage.module.scss';
import { ListContainer } from '../../containers';

// Pages map directly to Routes, i.e. one page equals on Route
// Handler that maps to a route in /utils/routes
const ListPage = (props) => (
  <section className={styles.container}>
    <ListContainer
      {...props}
    />
  </section>
);

export default cssModules(ListPage, styles);
