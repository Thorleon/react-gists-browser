import * as types from '../constants/gistsConstants';

const gists = (state = {
  isFetching: false,
  ownGists: [],
  starredGists: [],
  labels: {},
  currentGist: null,
  gist: null,
  searchGist: ""
}, action) => {

  const addLabelToGist = (gist) => {
    if (state.labels[gist.id] !== undefined) {
      gist["label"] = state.labels[gist.id];
      return gist;
    }
    else {
      gist["label"] = "";
      return gist;
    }
  };

  switch (action.type) {
    case types.LIST_PENDING || types.NEW_REQUEST
    || types.DELETE_REQUEST || types.GIST_REQUEST:
      return Object.assign({}, state, {
        isFetching: true
      });
    case types.LIST_FULFILLED:
      const ownGistWithLabels = action.payload[0].map(addLabelToGist);
      const starredGistsWithLabels = action.payload[1].map(addLabelToGist);
      return Object.assign({}, state, {
        isFetching: false,
        ownGists: ownGistWithLabels,
        starredGists: starredGistsWithLabels
      });
    case types.NEW_RESPONSE:
      const updatedLabels = state.labels;
      updatedLabels[action.payload.gist.id] = action.payload.label;
      return Object.assign({}, state, {
        isFetching: false,
        labels: updatedLabels
      });
    case types.EDIT_RESPONSE:
      const updatedLabelsEdit = state.labels;
      updatedLabelsEdit[action.payload.gist.id] = action.payload.label;
      return Object.assign({}, state, {
        isFetching: false,
        labels: updatedLabelsEdit
      });
    case types.DELETE_RESPONSE:
      const deletedIndex = state.ownGists.findIndex( x => x.id === action.payload.id);
      const deletedOwnGists = state.ownGists.filter((gist, index) => index !== deletedIndex);
      return Object.assign({}, state, {
        isFetching: false,
        ownGists: deletedOwnGists
      });
    case types.CHANGE_CURRENT_GIST:
      return Object.assign({}, state, {
        currentGist: action.payload.id
      });
    case types.GIST_RESPONSE:
      const gistWithLabel = addLabelToGist(action.payload.gist);
      console.log(gistWithLabel);
      return Object.assign({}, state, {
        gist: gistWithLabel
      });
    case types.CHANGE_SEARCH:
      return Object.assign({}, state, {
        searchGist: action.payload.searchGist
      });
    default:
      return state;
  }
};

export default gists;
