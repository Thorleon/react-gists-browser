import * as types from '../constants/userConstants';

const user = (state = {
  isAuthenticated: false,
  token: null
}, action) => {
  switch (action.type) {
    case types.USER_LOGIN:
      return Object.assign({}, state, {
        isAuthenticated: true,
        token: action.payload.token
      });
    case types.USER_LOGOUT:
      return Object.assign({}, state, {
        isAuthenticated: false,
        token: null
      });
    default:
      return state;
  }
};

export default user;
