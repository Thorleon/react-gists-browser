import * as types from '../constants/userConstants';

export const loginUser = (token) => {
  localStorage.setItem('token', token);
  return {
    type: types.USER_LOGIN,
    payload: {
      token: token
    }
  };
};

export const logoutUser = () => {
  localStorage.removeItem('token');
  return {
    'type': types.USER_LOGOUT
  };
};