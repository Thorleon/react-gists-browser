import * as types from '../constants/gistsConstants';
import store from '../store/store';

const getHeaders = () => {
  const { token } = store.getState().user;
  let headers = new Headers();
  headers.append('Authorization', `bearer ${token}`);
  headers.append('Content-Type', 'application/json');
  headers.append('Accept', 'application/json');
  return headers;
};

export const fetchList = () => {
  return dispatch => {
    const data = fetchAllGists();
    return dispatch(receiveList(data));
  };
};

export const newGist = (data) => {
  return dispatch => {
    dispatch(requestNewGist());
    return fetch('https://api.github.com/gists', {
      headers: getHeaders(),
      method: 'POST',
      body: JSON.stringify(data)
    })
      .then(req => req.json())
      .then(json => dispatch(responseNewGist(json, data.label)));
  };
};

export const editGist = (data, id) => {
  return dispatch => {
    dispatch(requestEditGist());
    return fetch(`https://api.github.com/gists/${id}`, {
      headers: getHeaders(),
      method: 'PATCH',
      body: JSON.stringify(data)
    })
      .then(req => req.json())
      .then(json => dispatch(responseEditGist(json, data.label)));
  };
};

export const deleteGist = (id) => {
  return dispatch => {
    dispatch(requestDeleteGist());
    return fetch(`https://api.github.com/gists/${id}`, {
      headers: getHeaders(),
      method: 'DELETE'
    })
      .then(() => dispatch(responseDeleteGist(id)));
  };
};

export const changeCurrentGist = (id) => ({
  type: types.CHANGE_CURRENT_GIST,
  payload: {
    id
  }
});

export const fetchGist = (id) => {
  return dispatch => {
    dispatch(requestGist());
    return fetch(`https://api.github.com/gists/${id}`, {
      headers: getHeaders()
    })
      .then(req => req.json())
      .then(json => dispatch(responseGist(json)))
  };
};

export const changeSearch = (searchGist) => ({
  type: types.CHANGE_SEARCH,
  payload: {
    'searchGist': searchGist
  }
});

const fetchAllGists = () => (
  Promise.all([fetchOwnGists(), fetchStarredGists()])
);

const fetchOwnGists = () => {
  return fetch('https://api.github.com/gists', {
    headers: getHeaders()
  })
    .then(req => req.json());
};

const fetchStarredGists = () => {
  return fetch('https://api.github.com/gists/starred', {
    headers: getHeaders()
  })
    .then(req => req.json());
};

const receiveList = (gists) => ({
  type: types.LIST,
  payload: gists
});

const requestNewGist = () => ({
  type: types.NEW_REQUEST
});

const responseNewGist = (gist, label) => ({
  type: types.NEW_RESPONSE,
  payload: {
    gist,
    label
  }
});

const requestEditGist = () => ({
  type: types.EDIT_REQUEST
});

const responseEditGist = (gist, label) => ({
  type: types.EDIT_RESPONSE,
  payload: {
    gist,
    label
  }
});

const requestDeleteGist = () => ({
  type: types.DELETE_REQUEST
});

const responseDeleteGist = (id) => ({
  type: types.DELETE_RESPONSE,
  payload: {
    id
  }
});

const requestGist = () => ({
  type: types.GIST_REQUEST
});

const responseGist = (gist) => ({
  type: types.GIST_RESPONSE,
  payload: {
    gist
  }
});