export const LIST_PENDING = 'LIST_PENDING';
export const LIST_FULFILLED = 'LIST_FULFILLED';
export const LIST = 'LIST';
export const NEW_REQUEST = 'NEW_REQUEST';
export const NEW_RESPONSE = 'NEW_RESPONSE';
export const EDIT_REQUEST = 'EDIT_REQUEST';
export const EDIT_RESPONSE = 'EDIT_RESPONSE';
export const DELETE_REQUEST = 'DELETE_REQUEST';
export const DELETE_RESPONSE = 'DELETE_RESPONSE';
export const CHANGE_CURRENT_GIST = 'CHANGE_CURRENT_GIST';
export const CHANGE_SEARCH = 'CHANGE_SEARCH';
export const GIST_REQUEST = 'GIST_REQUEST';
export const GIST_RESPONSE = 'GIST_RESPONSE';