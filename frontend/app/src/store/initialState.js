const initalState = {
  user: {
    isAuthenticated: false,
    token: null
  },
  gists: {
    isFetching: false,
    ownGists: [],
    starredGists: [],
    currentGist: null,
    labels: {},
    gist: null,
    searchGist: ""
  }
};

export default initalState;
