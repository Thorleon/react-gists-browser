import requests
from flask import Flask, request, render_template, redirect

app = Flask(__name__, static_folder="./frontend/app/build", template_folder="./frontend")

CLIENT_ID = '576899d00454f9480f56'
CLIENT_SECRET = '79bbb9a4a88669229e4efbe49a42132d6b493ede'


@app.route('/', methods=['GET'])
def index():
    return render_template('index.html')


@app.route('/get-token', methods=['GET'])
def get_token():
    code = request.args.get('code')
    headers = {'Accept': 'application/json'}
    r = requests.post(
            'https://github.com/login/oauth/access_token?client_id={}&client_secret={}&code={}&scope=repo%20gist'
            .format(CLIENT_ID, CLIENT_SECRET, code), headers=headers)
    token = r.json()['access_token']
    return redirect('/redirect?token={}'.format(token))


@app.route('/<path:path>', methods=['GET'])
def any_root_path(path):
    return render_template('index.html')

if __name__ == "__main__":
    app.run()